import time
import os
import sys

def cascade_affichage(sentence,speed = 0.05):
    # speed = 0.01
    question = sentence
    for character in question:
        sys.stdout.write(character)
        sys.stdout.flush()
        time.sleep(speed)

def print_earth():
    pe = "Planet earth: Such a wonderful planet..."
    cascade_affichage(pe)
    time.sleep(5)

    os.system('clear')
    print("                   ____                   ")
    print("              .-'\"\"p 8o\"\"`-.          ")
    print("           .-'8888P'Y.`Y[  '`-.           ")
    print("         ,']88888b.J8oo_      '`.         ")
    print("       ,' ,88888888888["+"         Y'.    ")
    print("      /   8888888888P            Y8\      ")
    print("     /    Y8888888P'             ]88\     ")
    print("    :     `Y88'   P              `888:    ")
    print("    :       Y8.oP '- >            Y88:    ")
    print("    |          `Yb  __             `'|    ")
    print("    :            `'d8888bo.          :    ")
    print("    :             d88888888ooo.      ;    ")
    print("     \            Y88888888888P     /     ")
    print("      \            `Y88888888P     /      ")
    print("       `.            d88888P'    ,'       ")
    print("         `.          888PP'    ,'         ")
    print("           `-.      d8P'    ,-'           ")
    print("              `-.,,_'__,,.-'              ")
    time.sleep(0.5)

    os.system('clear')
    print()
    print("                   ____                   ")
    print("              .-'\"\"p 8o\"\"`-.          ")
    print("           .-'8888P'Y.`Y[  '`-.           ")
    print("         ,']88888b.J8oo_      '`.         ")
    print("       ,' ,88888888888["+"         Y'.    ")
    print("      /   8888888888P            Y8\      ")
    print("     /    Y8888888P'             ]88\     ")
    print("    :     `Y88'   P              `888:    ")
    print("    :       Y8.oP '- >            Y88:    ")
    print("    |          `Yb  __             `'|    ")
    print("    :            `'d8888bo.          :    ")
    print("    :             d88888888ooo.      ;    ")
    print("     \            Y88888888888P     /     ")
    print("      \            `Y88888888P     /      ")
    print("       `.            d88888P'    ,'       ")
    print("         `.          888PP'    ,'         ")
    print("           `-.      d8P'    ,-'           ")
    print("              `-.,,_'__,,.-'              ")
    time.sleep(0.5)

    os.system('clear')
    print()
    print()
    print("                   ____                   ")
    print("              .-'\"\"p 8o\"\"`-.          ")
    print("           .-'8888P'Y.`Y[  '`-.           ")
    print("         ,']88888b.J8oo_      '`.         ")
    print("       ,' ,88888888888["+"         Y'.    ")
    print("      /   8888888888P            Y8\      ")
    print("     /    Y8888888P'             ]88\     ")
    print("    :     `Y88'   P              `888:    ")
    print("    :       Y8.oP '- >            Y88:    ")
    print("    |          `Yb  __             `'|    ")
    print("    :            `'d8888bo.          :    ")
    print("    :             d88888888ooo.      ;    ")
    print("     \            Y88888888888P     /     ")
    print("      \            `Y88888888P     /      ")
    print("       `.            d88888P'    ,'       ")
    print("         `.          888PP'    ,'         ")
    print("           `-.      d8P'    ,-'           ")
    print("              `-.,,_'__,,.-'              ")
    time.sleep(0.5)

    os.system('clear')
    print()
    print()
    print()
    print("                   ____                   ")
    print("              .-'\"\"p 8o\"\"`-.          ")
    print("           .-'8888P'Y.`Y[  '`-.           ")
    print("         ,']88888b.J8oo_      '`.         ")
    print("       ,' ,88888888888["+"         Y'.    ")
    print("      /   8888888888P            Y8\      ")
    print("     /    Y8888888P'             ]88\     ")
    print("    :     `Y88'   P              `888:    ")
    print("    :       Y8.oP '- >            Y88:    ")
    print("    |          `Yb  __             `'|    ")
    print("    :            `'d8888bo.          :    ")
    print("    :             d88888888ooo.      ;    ")
    print("     \            Y88888888888P     /     ")
    print("      \            `Y88888888P     /      ")
    print("       `.            d88888P'    ,'       ")
    print("         `.          888PP'    ,'         ")
    print("           `-.      d8P'    ,-'           ")
    print("              `-.,,_'__,,.-'              ")
    time.sleep(0.5)

    os.system('clear')
    pe1 = "The time has come for us to explore new planets."
    cascade_affichage(pe1)
    time.sleep(0.5)
    print()
    print()
    print()
    print("                   ____                   ")
    print("              .-'\"\"p 8o\"\"`-.          ")
    print("           .-'8888P'Y.`Y[  '`-.           ")
    print("         ,']88888b.J8oo_      '`.         ")
    print("       ,'     88888888888["+"      Y'.    ")
    print("      /       Y8888888888P       Y8\      ")
    print("     /      Y8888888P'            ]8\     ")
    print("    :     `Y888888888'       `8888888:    ")
    print("    :       Y88888888P '   Y888888888:    ")
    print("    |        `Y888b      `'8888888888|    ")
    print("    :          `Yb      `'d8888bo.   :    ")
    print("    :                    d88888888ooo;    ")
    print("     \ 8y               Y88888888888/     ")
    print("      \ 88]             `Y88888888P/      ")
    print("       `.888'              d88888,'       ")
    print("         `.8Y              888P,'         ")
    print("           `-.             8,-'           ")
    print("              `-.,,_'__,,.-'              ")
    time.sleep(3)
    os.system('clear')
    time.sleep(5)

print_earth()
