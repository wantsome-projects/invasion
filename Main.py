# from pygame import mixer
# mixer.init()
# mixer.music.load("sunet.mp3")
# mixer.music.play()
import sys
import os
import random
import time

# TODO: Difficulty lvl
# TODO: Skip Intro button
# TODO: New random events day - Increase max HP
# TODO: Weapon system
# TODO: Fix Stats
# TODO: Add Endgame Win ASCII ART

#### Global Variables ####
weeks_passed = 0
days_passed = 0
pod = True
pod_requirements = 30
pod_state = 0
scientist_left = 0
win_game = False
dd = 0
alien_hp = 30

def title_screen_selections():
    option = input("> ")
    if option.lower().rstrip() == ("play"):
        setup_game()
    elif option.lower().rstrip() == ("help"):
        help_menu()
    elif option.lower().rstrip() == ("exit"):
        sys.exit()

    while option.lower().rstrip() not in ["play","help","exit"]:
        print("Please enter a valid command.")
        option = input("> ")
        if option.lower().rstrip() == ("play"):
            setup_game()
        elif option.lower().rstrip() == ("help"):
            help_menu()
        elif option.lower().rstrip() == ("exit"):
            sys.exit()

def cascade_affichage(sentence,speed = 0.05):
    speed = 0.01
    question = sentence
    for character in question:
        sys.stdout.write(character)
        sys.stdout.flush()
        time.sleep(speed)

def title_screen():
    os.system('clear')
    print("################################################################")
    print("#                         ~ Invasi0n ~                         #")
    print("################################################################")
    print("#                           ~ Play ~                           #")
    print("#                           ~ Help ~                           #")
    print("#                           ~ Exit ~                           #")
    print("#                 C0pyright 2019 @Desdichshado                 #")
    print("################################################################")
    title_screen_selections()


def help_menu():
    os.system('clear')
    print("################################################################")
    print("#                       Invasi0n ~ Help                        #")
    print("################################################################")
    print("# ~ You are a space soldier and your base is invaded by nasty  #")
    print("#  aliens. You have to save everyone on the base(including     #")
    print("#  yourself) before you are overwhelmed.                       #")
    print("#                                                              #")
    print("# ~ The game has a night/day cycle. During day, you have       #")
    print("#  several actions to choose from in the 5 phases of the day.  #")
    print("#  At night the ever-increasing alien horde tries to kill you. #")
    print("#                                                              #")
    print("# ~ Good luck surviving and have fun!                          #")
    print("#                                                              #")
    print("################################################################")
    inp = input("Press Enter to return to Main Menu...")
    title_screen()


#### Win or Lose ####
def you_win():
    os.system('clear')
    print("Congratulations! You won the game and saved every scientist as well!")
    pname = myPlayer.name
    speech9 = pname + " returns home to a hero's welcome!\n"
    cascade_affichage(speech9)
    print("            _..._             ")
    print("          .'     '.      _    ")
    print("         /    .-\"\"-\   _/ \ ")
    print("       .-|   /:.   |  |   |   ")
    print("       |  \  |:.   /.-'-./    ")
    print("       | .-'-;:__.'    =/     ")
    print("       .'=  *=|NASA _.='      ")
    print("      /   _.  |    ;          ")
    print("     ;-.-'|    \   |          ")
    print("    /   | \    _\  _\         ")
    print("    \__/'._;.  ==' ==\        ")
    print("             \    \   |       ")
    print("             /    /   /       ")
    print("             /-._/-._/        ")
    print("             \   `\  \        ")
    print("              `-._/._/        ")
    inp = input("Press Enter to continue...")

    os.system('clear')
    speech10 = "As for the aliens...\n"
    cascade_affichage(speech10)
    print("                         .:'  ")
    print("                     _.::'    ")
    print("        .-;;-.      (_.'      ")
    print("       / ;;;' \               ")
    print("      |.  `:   |              ")
    print("       \:   `;/               ")
    print("        '-..-'                ")
    time.sleep(0.5)

    os.system('clear')
    print("                      .:'    ")
    print("                  _.::'      ")
    print("        .-;;-.   (_.'        ")
    print("       / ;;;' \              ")
    print("      |.  `:   |             ")
    print("       \:   `;/              ")
    print("        '-..-'               ")
    time.sleep(0.5)

    os.system('clear')
    print("                    .:'      ")
    print("                _.::'        ")
    print("        .-;;-. (_.'          ")
    print("       / ;;;' \              ")
    print("      |.  `:   |             ")
    print("       \:   `;/              ")
    print("        '-..-'               ")
    time.sleep(0.5)

    os.system('clear')
    print("                   .:'       ")
    print("         ....  _.::'         ")
    print("       .:-""##(_#'           ")
    print("     .:/    ####.            ")
    print("     :|        |:            ")
    print("     ':\      /:'            ")
    print("      '::-..-::'             ")
    print("        `''''`               ")
    time.sleep(0.5)

    os.system('clear')
    print("                .                   ")
    print("                .                   ")
    print("                :                   ")
    print("                !                   ")
    print("                |                   ")
    print("             ,` | `.                ")
    print("    -- --- --+-<#>-+- ---  --  -    ")
    print("             `._|_,'                ")
    print("                T                   ")
    print("                |                   ")
    print("                !                   ")
    print("                :                   ")
    print("                .                   ")
    time.sleep(0.5)

    os.system('clear')
    print("                .                   ")
    print("                ;                   ")
    print("            - --+-- -               ")
    print("                !                   ")
    print("                .                   ")
    time.sleep(0.5)

    os.system('clear')
    print("                .                   ")
    print("               -+-                  ")
    print("                .                   ")
    time.sleep(0.3)

    os.system('clear')
    print("                +                   ")
    time.sleep(0.2)



    os.system('clear')
    speech11 = "They quickly learned that they shouldn't mess with us.\n"
    cascade_affichage(speech11)
    print()
    inp = input("Press Enter to return to Main Menu...")
    title_screen()

def you_lose():
    os.system('clear')
    print("It's a sad day for earth and humanity. They know they are the next target.")
    print()
    print("                       ______                      ")
    print("                    .-*      *-.                   ")
    print("                   /            \                  ")
    print("       _          |              |          _      ")
    print("      ( \         |,  .-.  .-.  ,|         / )     ")
    print("       > *=._     | )(__/  \__)( |     _.=* <      ")
    print("      (_/*=._*=._ |/     /\     \| _.=*_.=*\_)     ")
    print("             *=._ (_     ^^     _)*_.=*            ")
    print("                 *=\__|IIIIII|__/=*                ")
    print("                _.=*| \IIIIII/ |*=._               ")
    print("      _     _.=*_.=*\          /*=._*=._     _     ")
    print("     ( \_.=*_.=*     `--------`     *=._*=._/ )    ")
    print("      > _.=*                            *=._ <     ")
    print("     (_/                                    \_)    ")
    print()
    inp = input("Press Enter to return to Main Menu...")
    title_screen()


#### Gather resoruces ####
def option_1():
    global scientist_left
    rnr = random.randint(1, 100)
    if rnr <= 80:
        rnr1 = random.randint(1, 3)
        print("You gathered " + str(rnr1 * 10) + " resources!")
        myPlayer.resources = myPlayer.resources + rnr1 * 10
    elif rnr > 80 and rnr <= 84:
        rnr1 = random.randint(2, 4)
        if rnr1 == 2:
            print("You stumble on an alien trap. \nYou lose " + str(rnr1 * 5) + " life.")
            myPlayer.HP = myPlayer.HP - rnr1 * 5
        elif rnr1 == 3:
            print("You stumble on an alien trap. \nYou're quick as a flash, and avoid the danger.")
        elif rnr1 == 4:
            print("You are attacked and chased by some angry aliens. \nYou are lucky to escape, but you lose " + str(rnr1 * 5) + " life.")
            myPlayer.HP = myPlayer.HP - rnr1 * 5
    elif rnr == 85 or rnr == 86:
        print("Searching trough the base you found one scientist hidden in a locker. \nYou decide to save him as well.")
        scientist_left += 1
    elif rnr > 86:
        rnr1 = random.randint(1, 3)
        if rnr1 == 1:
            print("You failed to find anything useful for building the escape pod.")
        elif rnr1 == 2:
            print("No luck finding any parts for the escape pod this time.")
        elif rnr1 == 3:
            print("Nothing in your search have proven useful for your pod construction.")

    inp = input("Press Enter to continue...")

#### Heal yourself ####
def option_2():
    rnr = random.randint(1, 13)
    if rnr <= 3:
        hp = 1
        if myPlayer.HP + hp * 10 > myPlayer.max_HP:
            myPlayer.HP = myPlayer.max_HP
            print("You found some painkillers. \nYou use them to heal " + str(myPlayer.max_HP - myPlayer.HP) + " HP.")
        else:
            print("You found some painkillers. \nYou use them to heal " + str(hp * 10) + " HP.")
            myPlayer.HP = myPlayer.HP + hp * 10
    elif rnr > 3 and rnr <= 6:
        hp = 2
        if myPlayer.HP + hp * 10 > myPlayer.max_HP:
            myPlayer.HP = myPlayer.max_HP
            print("You found some meds. \nYou use them to heal " + str(myPlayer.max_HP - myPlayer.HP) + " HP.")
        else:
            print("You found some meds. \nYou use them to heal " + str(hp * 10) + " HP.")
            myPlayer.HP = myPlayer.HP + hp * 10
    elif rnr > 6 and rnr <= 9:
        hp = 3
        print("You found a healing bot. You ask him to treat your wounds. \nHe helps you heal " + str(myPlayer.max_HP - myPlayer.HP) + " HP.")
        if myPlayer.HP + hp * 10 > myPlayer.max_HP:
            myPlayer.HP = myPlayer.max_HP
        else:
            print("You found a healing bot. You ask him to treat your wounds. \nHe helps you heal " + str(hp * 10) + " HP.")
            myPlayer.HP = myPlayer.HP + hp * 10

    else:
        rnr1 = random.randint(1,5)
        if rnr1 == 5:
            print("Searching for supplies, you found some angry aliens. \nYou manage to escape, but lose 10 life.")
            myPlayer.HP = myPlayer.HP - 10
        else:
            print("You found nothing useful in your search.")

    inp = input("Press Enter to continue...")

#### Pod ####
def option_3():
    global scientist_left, pod, pod_state
    if pod == True:
        if scientist_left == 0:
            you_win()
        else:
            scientist_left -= 1
            pod = False
    else:
        pod_state += 1
        myPlayer.resources -= 25
        if pod_state == 2:
            print("Your escape pod is ready for launch!")
            pod =  True
            pod_state = 0
        else:
            print("Your escape pod is half ready.")

#### Show stats ####
def option_4():
    global scientist_left
    os.system('clear')
    if dd == 0:
        print("       _..._                 ")
        print("     .::::. `.               ")
        print("    :::::::.  :    Dawn      ")
        print("    ::::::::  :              ")
        print("    `::::::' .'              ")
        print("      `'::'-'                ")
        print("Week: " + str(weeks_passed))
        print("Day: " + str(days_passed))
        print("Name: " + myPlayer.name)
        print("Max HP: " + str(myPlayer.max_HP))
        print("Current HP: " + str(myPlayer.HP))
        print("Resoruces: " + str(myPlayer.resources))
        print("Scientists left: " + str(scientist_left))
        inp = input("Press Enter to continue...")

    elif dd == 1:
        print("       _..._                 ")
        print("     .::'   `.               ")
        print("    :::       :    Morning   ")
        print("    :::       :              ")
        print("    `::.     .'              ")
        print("      `':..-'                ")
        print("Week: " + str(weeks_passed))
        print("Day: " + str(days_passed))
        print("Name: " + myPlayer.name)
        print("Max HP: " + str(myPlayer.max_HP))
        print("Current HP: " + str(myPlayer.HP))
        print("Resoruces: " + str(myPlayer.resources))
        print("Scientists left: " + str(scientist_left))
        inp = input("Press Enter to continue...")
    elif dd == 2:
        print("       _..._                 ")
        print("     .'     `.               ")
        print("    :         :    Noon      ")
        print("    :         :              ")
        print("    `.       .'              ")
        print("      `-...-'                ")
        print("Week: " + str(weeks_passed))
        print("Day: " + str(days_passed))
        print("Name: " + myPlayer.name)
        print("Max HP: " + str(myPlayer.max_HP))
        print("Current HP: " + str(myPlayer.HP))
        print("Resoruces: " + str(myPlayer.resources))
        print("Scientists left: " + str(scientist_left))
        inp = input("Press Enter to continue...")
    elif dd == 3:
        print("       _..._                 ")
        print("     .'   `::.               ")
        print("    :       :::    Afternoon ")
        print("    :       :::              ")
        print("    `.     .::'              ")
        print("      `-..:''                ")
        print("Week: " + str(weeks_passed))
        print("Day: " + str(days_passed))
        print("Name: " + myPlayer.name)
        print("Max HP: " + str(myPlayer.max_HP))
        print("Current HP: " + str(myPlayer.HP))
        print("Resoruces: " + str(myPlayer.resources))
        print("Scientists left: " + str(scientist_left))
        inp = input("Press Enter to continue...")
    elif dd == 4:
        print("       _..._                 ")
        print("     .' .::::.               ")
        print("    :  ::::::::    Dusk      ")
        print("    :  ::::::::              ")
        print("    `. '::::::'              ")
        print("      `-.::''                ")
        print("Week: " + str(weeks_passed))
        print("Day: " + str(days_passed))
        print("Name: " + myPlayer.name)
        print("Max HP: " + str(myPlayer.max_HP))
        print("Current HP: " + str(myPlayer.HP))
        print("Resoruces: " + str(myPlayer.resources))
        print("Scientists left: " + str(scientist_left))
        inp = input("Press Enter to continue...")
    elif dd == 5:
        print("       _..._                 ")
        print("     .:::::::.               ")
        print("    :::::::::::    Night     ")
        print("    :::::::::::              ")
        print("    `:::::::::'              ")
        print("      `':::''                ")
        print("Week: " + str(weeks_passed))
        print("Day: " + str(days_passed))
        print("Name: " + myPlayer.name)
        print("Max HP: " + str(myPlayer.max_HP))
        print("Current HP: " + str(myPlayer.HP))
        print("Resoruces: " + str(myPlayer.resources))
        print("Scientists left: " + str(scientist_left))
        inp = input("Press Enter to continue...")

#### Night Fight ####
def night_fight():
    global weeks_passed, alien_hp, nr_al
    os.system('clear')
    speech6 = "The night has come, you prepare for the alien Invasi0n!\n"
    cascade_affichage(speech6, 0.05)
    print("      .             *        .     .       .                    ")
    print("           .     _     .     .            .       .             ")
    print("    .    .   _  / |      .        .  *         _  .     .       ")
    print("            | \_| |                           | | __            ")
    print("          _ |     |                   _       | |/  |           ")
    print("         | \      |      ____        | |     /  |    \          ")
    print("         |  |     \    +/_\/_\+      | |    /   |     \         ")
    print("       _/____\--...\___ \_||_/ ___...|__\-..|____\____/__       ")
    print("          .     .      |_|__|_|         .       .               ")
    print("       .    . .       _/ /__\ \_ .          .                   ")
    print("          .       .    .           .         .                  ")
    print()
    inp = input("Press Enter to continue...")

    os.system('clear')
    speech7 = "You take your gun out and get ready to kick some alien ass!\n"
    cascade_affichage(speech7, 0.05)
    print("                                   _______                          ")
    print("                                  <((((((\\\\                       ")
    print("                                 /      . }\                        ")
    print("                                 ;--..--._|}                        ")
    print("              (\                 '--/\--'  )                        ")
    print("               \\\                | '-'  :'|                        ")
    print("                \\\               . -==- .-|                        ")
    print("                 \\\               \.__.'   \--._                   ")
    print("                 [\\\          __.--|       //  _/'--.              ")
    print("                 \ \\\       .'-._ ('-----'/ __/      \             ")
    print("                  \ \\\     /   __>|      | '--.       |            ")
    print("                   \ \\\   |   \   |     /    /       /             ")
    print("                    \ '\ /     \  |     |  _/       /               ")
    print("                     \  \       \ |     | /        /                ")
    print("                      \  \       \       /        /                 ")
    print()
    inp = input("Press Enter to continue...")
    nr_al = 0
    while nr_al == 0:
        rnr = random.randint(1,3)
        if rnr == 1:
            nr_al = rnr + weeks_passed - 1
            if nr_al == 1:
                print(str(rnr + weeks_passed - 1) + " alien is coming for you! \nPrepare to fight!" )
            else:
                print(str(rnr + weeks_passed - 1) + " aliens are coming for you! \nPrepare to fight!" )
        elif rnr == 2:
            nr_al = rnr + weeks_passed - 1
            print("Aliens are attacking you! I can count: " + str(rnr + weeks_passed - 1))
        elif rnr == 3 and weeks_passed > 2:
            nr_al = rnr + weeks_passed
            print("Aliens are swarming you! Watch out! They are: " + str(rnr + weeks_passed))

    horde = []

    for i in range(nr_al + 1):
        horde.append([i, 30])

    nr_al -= 1

    # del horde[nr_al]

    time.sleep(2)
    while myPlayer.HP > 0 or len(horde) > 0:
        rnr = random.randint(1,2)
        if rnr == 1:
            print("There are " + str(nr_al + 1) + " alien(s) left.")
            if nr_al + 1 == 0:
                break
            print("They attack first! You try to parry.")
            rnr1 =  random.randint(1,2)
            if rnr1 == 1:
                print("You succesfully parry the attack! Now it's your turn to strike back!")
                rnr2 =  random.randint(1,2)
                if rnr2 == 1:
                    print("It's a critical! You deal 9999999 damage to the alien.")
                    del horde[nr_al]
                    nr_al -= 1
                    if len(horde) == 0:
                        break
                    inp = input("Press Enter to continue...")
                else:
                    rnr3 = random.randint(15,30)
                    print("You manage to get a hit on the alien! That deals " + str(rnr3) + " damage to one of them!")
                    horde[nr_al][1] = horde[nr_al][1] - rnr3
                    if horde[nr_al][1] <= 0:
                        del horde[nr_al]
                        nr_al -= 1
                        print("There are " + str(nr_al + 1) + " aliens remaining!")
                        if len(horde) == 0:
                            break
                    else:
                        print("There are " + str(nr_al + 1) + " aliens remaining! \nOne of them has " + str(horde[nr_al][1]) + " life left.")
                    inp = input("Press Enter to continue...")
            else:
                print("You fail to parry the attack! Prepare for a hit!")
                rnr2 =  random.randint(5,10)
                print("You get hit for " + str(rnr2) + " damage!")
                myPlayer.HP -= rnr2
                inp = input("Press Enter to continue...")
        else:
            print("There are " + str(nr_al + 1) + " alien(s) left.")
            if nr_al + 1 == 0:
                break
            print("You get the first move!")
            rnr2 =  random.randint(1, 100)
            if rnr2 > 80:
                print("It's a critical! You deal 9999999 damage to the alien.")
                del horde[nr_al]
                nr_al -= 1
                if len(horde) == 0:
                    break
                inp = input("Press Enter to continue...")
            else:
                rnr3 = random.randint(15,30)
                print("You manage to get a hit on the alien! That deals " + str(rnr3) + " damage to one of them!")
                horde[nr_al][1] = horde[nr_al][1] - rnr3
                if horde[nr_al][1] <= 0:
                    del horde[nr_al]
                    nr_al -= 1
                    print("There are " + str(nr_al + 1) + " aliens remaining!")
                    if len(horde) == 0:
                        break
                else:
                    print("There are " + str(nr_al + 1) + " aliens remaining! \nOne of them has " + str(horde[nr_al][1]) + " life left.")
                inp = input("Press Enter to continue...")

    inp = input("Press Enter to continue...")
    if myPlayer.HP < 0:
        you_lose()
    else:
        os.system('clear')
        speech8 = "You survived the nigth! You live to fight another day!\n"
        cascade_affichage(speech8, 0.05)
        print("     .          \          .                         .                          ")
        print("          .      \   ,                                                          ")
        print("       .          o     .                 .                   .            .    ")
        print("         .         \                 ,             .                .           ")
        print("                   #\##\#      .                              .        .        ")
        print("                 #  #O##\###                .                        .          ")
        print("       .        #*#  #\##\###                       .                     ,     ")
        print("            .   ##*#  #\##\##               .                     .             ")
        print("          .      ##*#  #o##\#         .                             ,       .   ")
        print("              .    #*#  #\#     .                    .             .          , ")
        print("                          \          .                         .                ")
        print("    ____^/\___^--____/\____o______________/\/\---/\___________---______________ ")
        print("       /\^   ^  ^    ^                  ^^ ^  '\ ^          ^       ---         ")
        print("             --           -            --  -      -         ---  __       ^     ")
        print("       --  __                      ___--  ^  ^                         --  __   ")
        print()
        inp = input("Press Enter to continue...")

def validate_answer():
    option = input("> ")
    if option.lower().rstrip() == ("1"):
        option_1()
    elif option.lower().rstrip() == ("2") and myPlayer.HP != myPlayer.max_HP:
        option_2()
    elif (option.lower().rstrip() == ("3") and myPlayer.resources >= pod_requirements) or (option.lower().rstrip() == ("3") and pod == True):
        option_3()
    elif option.lower().rstrip() == ("4"):
        option_4()
    elif option.lower().rstrip() == ("exit"):
        print("Are you sure you want to exit? (y/n)")
        option = input("> ")
        if option.lower().strip() == "y":
            sys.exit()
        else:
            pass # TODO: TRebuie sa se intoarca la joc daca zic nu

    while option.lower() not in ["1","2","3","4","exit"]:
        print("Please enter a valid command.")
        option = input("> ")
        if option.lower().rstrip() == ("1"):
            option_1()
        elif option.lower().rstrip() == ("2") and myPlayer.HP != myPlayer.max_HP:
            option_2()
        elif (option.lower().rstrip() == ("3") and myPlayer.resources >= pod_requirements) or (option.lower().rstrip() == ("3") and pod == True):
            option_3()
        elif option.lower().rstrip() == ("4"):
            option_4()
        elif option.lower().rstrip() == ("exit"):
            print("Are you sure you want to exit? (y/n)")
            option = input("> ")
            if option.lower().strip() == "y":
                sys.exit()
            else:
                pass # TODO: TRebuie sa se intoarca la joc daca zic nu
    return option.lower().rstrip()

def choose_action():
    print("Scientists left: " + str(scientist_left))
    print("Choose one action: ")
    print("1. Gather resources.")
    if myPlayer.HP == myPlayer.max_HP:
        print("2. You are already at full HP.")
    else:
        print("2. Heal yourself. (" + str(myPlayer.HP) + "HP / " + str(myPlayer.max_HP) + "HP)")
    if pod == True:
        print("3. Send the escape pod back to earth.")
    else:
        if myPlayer.resources >= pod_requirements:
            print("3. Build an escape pod consuming " + str(pod_requirements) + " resources. (" + str(pod_state) + "/2)")
        else:
            print("3. You have insufficient resources to work on the escape pod.")
    print("4. Show stats.")
    return validate_answer()


def day_dawn():
    while True:
        os.system('clear')
        print("Day: " + str(days_passed))
        print("       _..._                 ")
        print("     .::::. `.               ")
        print("    :::::::.  :    Dawn      ")
        print("    ::::::::  :              ")
        print("    `::::::' .'              ")
        print("      `'::'-'                ")
        print()
        option = choose_action()
        if option != "4":
            break

def day_morning():
    while True:
        os.system('clear')
        print("Day: " + str(days_passed))
        print("       _..._                 ")
        print("     .::'   `.               ")
        print("    :::       :    Morning   ")
        print("    :::       :              ")
        print("    `::.     .'              ")
        print("      `':..-'                ")
        print()
        option = choose_action()
        if option != "4":
            break

def day_noon():
    while True:
        os.system('clear')
        print("Day: " + str(days_passed))
        print("       _..._                 ")
        print("     .'     `.               ")
        print("    :         :    Noon      ")
        print("    :         :              ")
        print("    `.       .'              ")
        print("      `-...-'                ")
        print()
        option = choose_action()
        if option != "4":
            break

def day_afternoon():
    while True:
        os.system('clear')
        print("Day: " + str(days_passed))
        print("       _..._                 ")
        print("     .'   `::.               ")
        print("    :       :::    Afternoon ")
        print("    :       :::              ")
        print("    `.     .::'              ")
        print("      `-..:''                ")
        print()
        option = choose_action()
        if option != "4":
            break

def day_dusk():
    while True:
        os.system('clear')
        print("Day: " + str(days_passed))
        print("       _..._                 ")
        print("     .' .::::.               ")
        print("    :  ::::::::    Dusk      ")
        print("    :  ::::::::              ")
        print("    `. '::::::'              ")
        print("      `-.::''                ")
        print()
        option = choose_action()
        if option != "4":
            break

def day_night():
    os.system('clear')
    night_fight()

def main_game_loop():
    global days_passed, weeks_passed, dd
    os.system('clear')
    while myPlayer.game_over is False:
        # dawn - morning - noon - afternoon - dusk - night
        for game_phase in range(0,6):
            if game_phase == 0:
                dd = game_phase
                days_passed += 1
                if days_passed % 7 == 0:
                    weeks_passed += 1
                day_dawn()
            elif game_phase == 1:
                dd = game_phase
                day_morning()
            elif game_phase == 2:
                dd = game_phase
                day_noon()
            elif game_phase == 3:
                dd = game_phase
                day_afternoon()
            elif game_phase == 4:
                dd = game_phase
                day_dusk()
            elif game_phase == 5:
                dd = game_phase
                day_night()
            if myPlayer.HP <= 0:
                myPlayer.game_over = True
                you_lose()


def print_earth():
    os.system('clear')
    time.sleep(0.5)
    pe = "Planet earth: Such a wonderful planet...\n"
    cascade_affichage(pe)
    time.sleep(3)

    os.system('clear')
    print("                   ____                   ")
    print("              .-'\"\"p 8o\"\"`-.          ")
    print("           .-'8888P'Y.`Y[  '`-.           ")
    print("         ,']88888b.J8oo_      '`.         ")
    print("       ,' ,88888888888["+"         Y'.    ")
    print("      /   8888888888P            Y8\      ")
    print("     /    Y8888888P'             ]88\     ")
    print("    :     `Y88'   P              `888:    ")
    print("    :       Y8.oP '- >            Y88:    ")
    print("    |          `Yb  __             `'|    ")
    print("    :            `'d8888bo.          :    ")
    print("    :             d88888888ooo.      ;    ")
    print("     \            Y88888888888P     /     ")
    print("      \            `Y88888888P     /      ")
    print("       `.            d88888P'    ,'       ")
    print("         `.          888PP'    ,'         ")
    print("           `-.      d8P'    ,-'           ")
    print("              `-.,,_'__,,.-'              ")
    time.sleep(0.5)

    os.system('clear')
    print()
    print("                   ____                   ")
    print("              .-'\"\"p 8o\"\"`-.          ")
    print("           .-'8888P'Y.`Y[  '`-.           ")
    print("         ,']88888b.J8oo_      '`.         ")
    print("       ,' ,88888888888["+"         Y'.    ")
    print("      /   8888888888P            Y8\      ")
    print("     /    Y8888888P'             ]88\     ")
    print("    :     `Y88'   P              `888:    ")
    print("    :       Y8.oP '- >            Y88:    ")
    print("    |          `Yb  __             `'|    ")
    print("    :            `'d8888bo.          :    ")
    print("    :             d88888888ooo.      ;    ")
    print("     \            Y88888888888P     /     ")
    print("      \            `Y88888888P     /      ")
    print("       `.            d88888P'    ,'       ")
    print("         `.          888PP'    ,'         ")
    print("           `-.      d8P'    ,-'           ")
    print("              `-.,,_'__,,.-'              ")
    time.sleep(0.5)

    os.system('clear')
    print()
    print()
    print("                   ____                   ")
    print("              .-'\"\"p 8o\"\"`-.          ")
    print("           .-'8888P'Y.`Y[  '`-.           ")
    print("         ,']88888b.J8oo_      '`.         ")
    print("       ,' ,88888888888["+"         Y'.    ")
    print("      /   8888888888P            Y8\      ")
    print("     /    Y8888888P'             ]88\     ")
    print("    :     `Y88'   P              `888:    ")
    print("    :       Y8.oP '- >            Y88:    ")
    print("    |          `Yb  __             `'|    ")
    print("    :            `'d8888bo.          :    ")
    print("    :             d88888888ooo.      ;    ")
    print("     \            Y88888888888P     /     ")
    print("      \            `Y88888888P     /      ")
    print("       `.            d88888P'    ,'       ")
    print("         `.          888PP'    ,'         ")
    print("           `-.      d8P'    ,-'           ")
    print("              `-.,,_'__,,.-'              ")
    time.sleep(0.5)

    os.system('clear')
    print()
    print()
    print()
    print("                   ____                   ")
    print("              .-'\"\"p 8o\"\"`-.          ")
    print("           .-'8888P'Y.`Y[  '`-.           ")
    print("         ,']88888b.J8oo_      '`.         ")
    print("       ,' ,88888888888["+"         Y'.    ")
    print("      /   8888888888P            Y8\      ")
    print("     /    Y8888888P'             ]88\     ")
    print("    :     `Y88'   P              `888:    ")
    print("    :       Y8.oP '- >            Y88:    ")
    print("    |          `Yb  __             `'|    ")
    print("    :            `'d8888bo.          :    ")
    print("    :             d88888888ooo.      ;    ")
    print("     \            Y88888888888P     /     ")
    print("      \            `Y88888888P     /      ")
    print("       `.            d88888P'    ,'       ")
    print("         `.          888PP'    ,'         ")
    print("           `-.      d8P'    ,-'           ")
    print("              `-.,,_'__,,.-'              ")
    time.sleep(0.9)

    os.system('clear')
    pe1 = "The time has come for us to explore new planets.\n"
    cascade_affichage(pe1)
    time.sleep(0.5)
    print()
    print()
    print()
    print("                   ____                   ")
    print("              .-'\"\"p 8o\"\"`-.          ")
    print("           .-'8888P'Y.`Y[  '`-.           ")
    print("         ,']88888b.J8oo_      '`.         ")
    print("       ,'     88888888888["+"      Y'.    ")
    print("      /       Y8888888888P       Y8\      ")
    print("     /      Y8888888P'            ]8\     ")
    print("    :     `Y888888888'       `8888888:    ")
    print("    :       Y88888888P '   Y888888888:    ")
    print("    |        `Y888b      `'8888888888|    ")
    print("    :          `Yb      `'d8888bo.   :    ")
    print("    :                    d88888888ooo;    ")
    print("     \ 8y               Y88888888888/     ")
    print("      \ 88]             `Y88888888P/      ")
    print("       `.888'              d88888,'       ")
    print("         `.8Y              888P,'         ")
    print("           `-.             8,-'           ")
    print("              `-.,,_'__,,.-'              ")
    time.sleep(3)
    os.system('clear')
    pe2 = "What we didn't know was that this new planet was a trap...\n"
    cascade_affichage(pe2, 0.1)
    time.sleep(4)

class player:
    def __init__(self):
        self.name = ''
        self.max_HP = 150
        self.HP = 150
        self.resources = 0
        self.game_over = False

myPlayer = player()

def setup_game():
    os.system('clear')
    question0 = "Would you like to skip the Intro? (y/n)\n"
    cascade_affichage(question0)

    option = input("> ")
    if option.lower().rstrip() == ("n"):
        print_earth()
    elif option.lower().rstrip() == ("y"):
        print(":(")
    else:
        while option.lower().rstrip() not in ["y","n"]:
            print("Please enter a valid command.")
            option = input("> ")

    os.system('clear')

    #### Name Selection ####
    question1 = "Hello player, what's your name?\n"
    cascade_affichage(question1)
    player_name = input("> ")
    myPlayer.name = player_name

    #### Introduction ####
    question3 = "Well, " + player_name + ", we are very lucky to have you here!\n"
    cascade_affichage(question3)

    speech1 = "There's not much time! Our base is going to be owerwhelmed by alien inavders!\n"
    speech2 = "You have to do something, or we are all doomed. \n"
    speech3 = "Luckily they can only come at night, that is because the sunlight burns their skin. \n"
    speech4 = "Do you have what it takes to save all the astronauts on the base and survive? \n"
    speech5 = "It's an INVASI0N!\n"
    cascade_affichage(speech1,0.06)
    cascade_affichage(speech2,0.05)
    cascade_affichage(speech3,0.04)
    cascade_affichage(speech4,0.03)
    cascade_affichage(speech5,0.2)
    time.sleep(0.5)
    question4 = "Are you ready?\n"
    cascade_affichage(question4)
    print("y/n")
    raspuns = input("> ")
    while raspuns.lower().rstrip() != 'y':
        print("You have to be ready! Are you ready now?\n")
        raspuns = input("> ")

    time.sleep(0.5)
    os.system('clear')
    print("##########################")
    print("#    Let's start now!    #")
    print("##########################")
    print()
    print("################################")
    print("# Here begins the adventure... #")
    print("################################")

    main_game_loop()

title_screen()
